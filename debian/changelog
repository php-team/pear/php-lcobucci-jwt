php-lcobucci-jwt (5.5.0-1) unstable; urgency=medium

  [ Filippo Tessarotto ]
  * Add migration API for `readonly` classes

 -- David Prévot <taffit@debian.org>  Thu, 30 Jan 2025 06:54:11 +0100

php-lcobucci-jwt (5.4.2-1) unstable; urgency=medium

  [ Anatoly Pashin ]
  * Mark Builder methods as pure
  * Revert interface changes, mark implementation methods

  [ Sebastiaan Knijnenburg ]
  * Add SensitiveParameter attribute to sensitive parameters

 -- David Prévot <taffit@debian.org>  Fri, 15 Nov 2024 14:33:47 +0100

php-lcobucci-jwt (5.4.0-1) unstable; urgency=medium

  [ Luís Cobucci ]
  * Drop PHP 8.1
  * Bump lcobucci/clock
  * Upgrade PHPUnit
  * Use attributes on all tests

  [ Filippo Tessarotto ]
  * Benchmarks: add EdDSA algorithm

  [ Cédric Anne ]
  * Add PHP 8.4 support

  [ David Prévot ]
  * Revert "Force system dependencies loading"
  * Adapt build to updated phpabtpl(1)

 -- David Prévot <taffit@debian.org>  Fri, 11 Oct 2024 09:26:33 +0100

php-lcobucci-jwt (5.3.0-1) unstable; urgency=medium

  [ Freebuu ]
  * add HasClaim constraint

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sat, 13 Apr 2024 14:55:37 +0200

php-lcobucci-jwt (5.2.0-2) unstable; urgency=medium

  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Wed, 06 Mar 2024 17:44:46 +0100

php-lcobucci-jwt (5.2.0-1) unstable; urgency=medium

  [ Luís Cobucci ]
  * Introduce fake implementation of signer for tests
  * Add a way to verify a token against multiple algorithms/keys

 -- David Prévot <taffit@debian.org>  Tue, 21 Nov 2023 07:56:21 +0100

php-lcobucci-jwt (5.1.0-1) unstable; urgency=medium

  [ Luís Cobucci ]
  * Update README.md

  [ javer ]
  * Support PHP 8.3

  [ David Prévot ]
  * Set upstream metadata fields: Repository.

 -- David Prévot <taffit@debian.org>  Wed, 01 Nov 2023 08:18:48 +0100

php-lcobucci-jwt (5.0.0-1) unstable; urgency=medium

  * Initial release (php-symfony-mercure-bundle dependency)

 -- David Prévot <taffit@debian.org>  Wed, 24 May 2023 12:13:55 +0200
